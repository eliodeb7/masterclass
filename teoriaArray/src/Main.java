import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        //instanciar array estatica
        String[] arraysStatic = new String[5];

        //rellenar
        arraysStatic[0] = "hola";
        arraysStatic[1] = "hr";
        arraysStatic[2] = "er";

        for (int i = 0; i < arraysStatic.length; i++) {
            arraysStatic[i] += ".";
        }



        //Arraylist
        ArrayList<Integer> arrayDinamica = new ArrayList<>();
        arrayDinamica.add(1);
        arrayDinamica.add(2);
        arrayDinamica.add(100);

        for (Integer numeros:arrayDinamica) {
            System.out.println(numeros);
            numeros += 5;
            System.out.println(numeros);
        }

        for (Integer numeros:arrayDinamica) {
            if (numeros == 6) {
                numeros = 1000;
            }
            System.out.println(numeros);
        }



    }
}
