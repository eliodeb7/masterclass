package manager;

import model.Car;

import java.util.ArrayList;
import java.util.List;

public class Manager {
    //Attribute of itself
    private static Manager manager;
    //List of cars
    private List<Car> listOfCars;

    //If is empty create an instance of the object
    /*With this function we ensure that we only create 1 object of this class*/
    public static void getInstance() {
        if (manager == null) {
            manager = new Manager();
        }
    }

    //Here we put the code that we need to run
    public Manager() {
        //Initzialize all elements needed
        init();

        //Run program
        run();
    }

    public void init() {
        //Start Array
        this.listOfCars = new ArrayList<>();

        //Add 2 object car to the arraylist
        Car bbw = new Car("1234 ABC", 5, 300);
        this.listOfCars.add(bbw);

        this.listOfCars.add(new Car("4321 CBA", 3, 500));

    }

    public void run() {
        this.listOfCars.get(0).setLicensePlate("3214 TRG");

        //Print all cars of array with for i
        System.out.println("#################### FOR I #################### ");
        for (int i = 0; i < this.listOfCars.size(); i++) {
            System.out.println(listOfCars.get(i));
        }
        System.out.println();


        //Change the seats of the first car
        this.listOfCars.get(0).setSeats(100);

        //Print all cars of array with for each
        System.out.println("#################### FOR EACH #################### ");
        for (Car car : this.listOfCars) {
            System.out.println(car);
        }
    }

}
