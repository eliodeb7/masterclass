package model;

public class Car {
    //Attributes of the class
    /*Is define private for so yo can manage the data from functions like get an set*/
    private String licensePlate;
    private int seats;
    private int CC;

    public Car() { }

    //Constructor, define how to create a object
    public Car(String licensePlate, int seats, int CC) {
        this.licensePlate = licensePlate;
        this.seats = -1;
        this.CC = CC;
    }

    public static void printJEJE() {
        System.out.println("jeje");
    }


    public String getLicensePlate() {
        return this.licensePlate;
    }

    public void setLicensePlate(String newLicensePlate) {
        this.licensePlate = newLicensePlate;
    }

    //Get the number of seats
    public int getSeats() {
        return this.seats;
    }

    //Change the number of seats
    public void setSeats(int seats) {
        this.seats = seats;
    }

    public void setSeats1() {
        this.seats = 1;
    }

    //IMPORTANT(SALVA CULOS) It helps to give a text to the object, if this function is not define it will print something like @Object12352314

   public String toString() {
        return "License Plate: " + this.licensePlate;
    }


}
